module DeviseHelper
  def devise_error_messages!
    return "" unless devise_error_messages?
    resource.errors.full_messages.map { |msg| "<b>#{msg}</b><br/>" }.join.html_safe
  end

  def devise_error_messages?
    !resource.errors.empty?
  end
  
  def devise_error_messages?
    !resource.errors.empty?
  end
end
