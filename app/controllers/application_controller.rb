class ApplicationController < ActionController::Base
	protect_from_forgery

	protected
	def after_sign_in_path_for(resource)

		if resource.class == Organizer
			sign_in_url = organizer_path(resource.id)
		elsif resource.class == Sponsor
			sign_in_url = ride_requests(resource.lyft_access_token)
		else
			# what goes here?
		end

		if request.referer == sign_in_url
			super
		else
			sign_in_url || request.referer || root_path
		end
	end
end
