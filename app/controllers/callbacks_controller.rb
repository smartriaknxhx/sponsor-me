class CallbacksController < Devise::OmniauthCallbacksController
  skip_before_action :verify_authenticity_token, only: :lyft

  def lyft
    @sponsor = Sponsor.from_omniauth(request.env["omniauth.auth"])
    sign_in @sponsor
    redirect_to ride_requests_path
  end
end
