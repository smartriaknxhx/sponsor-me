class RequestsController < ApplicationController
  def ride
    # params[:phone_number]
  end

  def sponsor_ride
    @request = RideRequest.find(params[:request_id])
    @request.status = 'sponsored'
    if @request.save
      flash[:success] = "Thank you for sponsoring this ride. We will inform you when the ride is complete."
      redirect_to ride_requests_path
    end
  end
end
