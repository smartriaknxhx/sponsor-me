class RidersController < ApplicationController

  def index
    @riders = Rider.includes(:ride_requests)
  end
  
  def new
    @rider = Rider.new
  end

  def create
    @rider = Rider.create(phone_number: params[:rider][:phone_number].gsub(/\D/, ""),
                          first_name: params[:rider][:first_name],
                          last_name: params[:rider][:last_name])
    if @rider.save
      redirect_to riders_path, notice: "Rider successfully added"
    else
      redirect_to new_rider_path, notice: "Something went wrong"
    end

  end

  def request_ride
    phone_number = params[:phone_number].gsub(/\D/, "")
    @rider = Rider.find_by(phone_number: phone_number)
    if @rider.nil?
      redirect_to root_path, notice: "Sorry, we could not find that number"
    end
    @ride = RideRequest.new
  end

  def create_ride_request
    @ride = RideRequest.new(
      rider_id: params[:id],
      status: 'open',
      start_time: params[:pickupTime],
      start_loc_name: params[:start][:name],
      start_street_address: params[:start][:address],
      start_city: params[:start][:city],
      start_zip: params[:start][:zip],
      dest_loc_name: params[:destination][:name],
      dest_street_address: params[:destination][:address],
      dest_city: params[:destination][:city],
      dest_zip: params[:destination][:zip]
    )

    ride_coord = RideCoord.new
    ride_coord.ride_request_id = @ride.id
    start_address = "#{@ride.start_street_address} #{@ride.start_city} #{@ride.start_zip}"
    end_address = "#{@ride.dest_street_address} #{@ride.dest_city} #{@ride.dest_zip}"
    map = GoogleMapApi.new
    start_lat_long = map.get_lat_long(start_address) rescue start_failure = true
    end_lat_long = map.get_lat_long(end_address) rescue dest_failure = true

    if dest_failure
      render json: { page: "destination", message: "Your destination address is incorrect, please enter a valid address" } and return
    end

    if start_failure
      render json: { page: "start", message: "Your starting address is incorrect, please enter a valid address" } and return
    end

    ride_coord.start_lat = start_lat_long[:lat]
    ride_coord.start_long = start_lat_long[:long]
    ride_coord.destination_lat = end_lat_long[:lat]
    ride_coord.destination_long = end_lat_long[:long]

    if @ride.save
      ride_coord.ride_request_id = @ride.id
      if ride_coord.save
        flash[:success] = "Your ride has been requested. We will notify you when there is a sponsor" 
        render json: { redirect_to: root_path }, status: :accepted
      else
        @ride.destroy
        render json: {}, status: :unprocessable_entity
      end
    else
      render json: {}, status: :unprocessable_entity
    end
  end
end
