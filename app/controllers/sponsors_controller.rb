class SponsorsController < ApplicationController

	def show
		@sponsor = Sponsor.where(lyft_access_token: params[:id]).first
	end

  # This can definitely be improved. Look at again tomorrow, seems to work for now
  # Also, when we have Concierge access, see if we can add the estimated_cost
  # to the ride request when it's created. Currently we need a sponsor to hit the ride_estimate
  # endpoint.
	def ride_requests
		lyft = LyftApi.new(current_sponsor.lyft_access_token)
		@ride_requests = RideRequest.all
		@ride_requests.each do |rr|
			ride_coord = RideCoord.where(ride_request_id: rr.id).first
			response = lyft.get_ride_estimate(ride_coord.start_lat, ride_coord.start_long, ride_coord.destination_lat, ride_coord.destination_long)
			response.parsed_response['cost_estimates'].each do |ce|
				if ce['ride_type'] == 'lyft'
					rr.estimated_cost = (ce["estimated_cost_cents_min"] + ce["estimated_cost_cents_max"]) / 2 / 100
					rr.save
				end
			end
		end
	end

end