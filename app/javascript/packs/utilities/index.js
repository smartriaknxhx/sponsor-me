/**
 * Retrieves the data from data-tags on an elem and removes them from DOM. All tags must be lower case
 * @param {(string|Object)} elem - The ID or DOM object to containing the data tags
 * @param {string[]} tags - A list of data-tags to find, minus the "data-"
 * @returns {Object} An object containing the tag as the key and value of that tag
 * @example
 * // Finds "data-mytag" and "data-my-tag" on elem with ID "myElem" and returns the cooresponding data
 * // Returns { "mytag": "VALUE", "my-tag": "VALUE" }
 * getData("#myElem", ["mytag", "my-tag"])
 * 
 * // Finds "data-myTag" and "data-my-tag" on elem with ID "myElem" and returns the data
 * // If mytag is not defined on the data, it will return the default value of false
 * getData("#myElem", [["mytag", false], "my-tag"])
 */
export function getData(elem, tags) {
  let data = {};
  elem = $(elem);

  _.each(tags, (tag) => {
    let defaultValue = undefined;

    // Check to see if we are using a default value
    if (_.isArray(tag)) {
      defaultValue = tag[1];
      tag = tag[0];
    }

    // Make sure to remove the "data-" in case it was added
    if (_.startsWith(tag, "data-")) {
      tag = _.trimStart(tag, "data-");
    }

    // case insensitive (because DOM is)
    tag = _.toLower(tag);
    let value = elem.data(tag);

    // Only add it if we have a value or the defaultValue is set
    if (!_.isUndefined(value) || !_.isUndefined(defaultValue)) {
      data[tag] = value || defaultValue;
    }

    elem.removeAttr(`data-${tag}`);
  });
  return data;
}