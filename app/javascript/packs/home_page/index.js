import Vue from "vue/dist/vue.esm";
import {
  getData
} from "../utilities";
import Axios from "axios";

export default function (elem) {
  const appProfile = new Vue({
    el: elem,
    data: {
      selectedUserType: "rider",
      newUser: {
        email: "",
        password: "",
        passwordConfirm: ""
      }
    },
    computed: {
      disableSignUpButton: function () {
        // Is Email Empty
        if (_.isEmpty(this.newUser.email)) return true;

        // Is Password Empty
        if (_.isEmpty(this.newUser.password)) return true;

        // Is Confirm Password Empty
        if (_.isEmpty(this.newUser.passwordConfirm)) return true;

        // Do passwords match
        if (!this.doSignupPasswordsMatch) return true;

        // Is it at least 6 characters
        if (this.newUser.password.length < 6) return true;

        return false;
      },

      doSignupPasswordsMatch: function () {
        return this.newUser.password === this.newUser.passwordConfirm;
      },

      showPasswordsDoNotMatch: function () {
        return !_.isEmpty(this.newUser.password) && !_.isEmpty(this.newUser.passwordConfirm) && !this.doSignupPasswordsMatch;
      }
    }
  });
}