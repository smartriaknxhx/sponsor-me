/* eslint no-console:0 */

import axios from "axios";
import Vue from "vue/dist/vue.esm";
import DatePicker from 'vue2-datepicker';
import "babel-polyfill";

// Import your Vue apps here
import homePageApp from "./home_page";
import requestRide from "./request_ride";

Vue.component('datepicker', DatePicker);

$(window).on("load", function (e) {
  // Send CSRF Token with all requests
  axios.defaults.headers.common['X-CSRF-Token'] = document.querySelector("meta[name=csrf-token]").content;

  // Allow request.xhr? to work on Rails
  axios.defaults.headers.common['X-Requested-With'] = "XMLHttpRequest";

  // Decide which app to instantiate
  switch (true) {
    case (!_.isEmpty($("#vue-app-home-page"))):
      homePageApp("#vue-app-home-page");
      break;
    case (!_.isEmpty($("#vue-app-request-ride"))):
      requestRide("#vue-app-request-ride");
      break;
  }
});