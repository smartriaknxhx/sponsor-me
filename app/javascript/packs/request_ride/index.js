import Vue from "vue/dist/vue.esm";
import {
  getData
} from "../utilities";
import Axios from "axios";

export default function (elem) {
  const data = getData(elem, ["id"])
  new Vue({
    el: elem,
    data: {
      pickupTime: null,
      currentPage: "destination",
      selectedTimeType: "asap",
      destination: {
        name: "",
        address: "",
        city: "",
        zip: ""
      },
      start: {
        name: "",
        address: "",
        city: "",
        zip: ""
      },
      processing: false
    },
    computed: {
      disableNextButton: function () {
        // If any value of destination is empty, do not allow to continue
        if (_.some(this.destination, (key) => _.isEmpty(key))) return true;
        return false;
      },
      disableRequestButton: function () {
        if (this.disableNextButton) return true;

        // If any value of the start is empty, do not allow to continue
        if (_.some(this.start, (key) => _.isEmpty(key))) return true;

        // If we've picked a custom time but not actually a time
        if (this.selectedTimeType === "custom" && _.isNull(this.pickupTime)) return true;

        return false;
      }
    },
    methods: {
      switchPage: function (nextPage) {
        this.currentPage = nextPage;
      },
      requestRide: function () {
        if (this.disableRequestButton) {
          return UIkit.notification({
            message: "Please fill out every field",
            status: "danger"
          });
        }

        this.processing = true;

        Axios({
          url: "/create_ride_request",
          method: "post",
          data: {
            id: data.id,
            start: this.start,
            destination: this.destination,
            pickupTime: this.selectedTimeType === "custom" ? this.pickupTime : new Date()
          }
        }).then((response) => {
          console.log(response);
          if (response.status === 202) {
            window.location.href = response.data.redirect_to;
          } else if (response.data.message) {
            UIkit.notification({
              message: response.data.message,
              status: "warning"
            });

            this.switchPage(response.data.page);
            this.processing = false;
          }
        }).catch((error) => {
          console.log(error);

          UIkit.notification({
            message: "An error occurred, please try again later",
            status: "danger"
          });

          this.processing = false;
        });
      }
    }
  });
}