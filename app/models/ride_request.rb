class RideRequest < ApplicationRecord

  belongs_to :rider
  STATUS_TYPES = ["sponsored", "open", "completed", "expired"]

end
