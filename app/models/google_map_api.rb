class GoogleMapApi < ApplicationRecord
  include HTTParty

  # TODO: move these to environmental variables
  MAP_URL = 'https://maps.googleapis.com/maps/api/geocode/json?'
  API_KEY = 'AIzaSyAb_dod9JEdVoI8k8yejeUuYEWAzPYHibU'

  # Given an address in a specific format, this will return an object with lat and long
  # Example: map.get_lat_long('8200 Kingston Pike, Knoxville, TN') will return {:lat=>35.9240426, :long=>-84.0536209}
  def get_lat_long(address)
    response = HTTParty.get("#{MAP_URL}address=#{address}&key=#{API_KEY}").parsed_response
    return {lat: response['results'].first['geometry']['location']['lat'], long: response['results'].first['geometry']['location']['lng']}
  end
end
