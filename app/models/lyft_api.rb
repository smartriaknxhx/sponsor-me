class LyftApi < ApplicationRecord
  include HTTParty

  # https://www.lyft.com/developers/apps/vgPzbiYBiZQn to access our lyft app

  # TODO: Move these to enviromental variables
  CLIENT_ID = ENV<client_id>
  CLIENT_SECRET = ENV<client_secret>
  LYFT_URL = 'https://api.lyft.com/v1/'

  def initialize(token)
    @access_token = token
  end

  def cancel_ride(ride_id)
    response = HTTParty.post("#{LYFT_URL}rides/#{ride_id}/cancel",
                             :headers => {"Authorization" => "Bearer #{@access_token}"})
  end

  # Returns everything about the ride. Can be used to update status and also inform rider of the vehicle
  # information (when available). Route_URL could be useful to track the rider if the sponsor wants to.
  def get_ride_details(ride_id)
    response = HTTParty.get("#{LYFT_URL}rides/#{ride_id}",
                            :headers => {"Authorization" => "Bearer #{@access_token}"})
  end

  # Returns a response with the ride_id, we need to make sure we save this id on the ride request model
  def request_ride(origin, destination)
    options = {"ride_type": "lyft", "origin": {"address": origin}, "destination": {"address": destination}}
    response = HTTParty.post("#{LYFT_URL}rides/",
                             :headers => {"Authorization" => "Bearer #{@access_token}"},
                             :body => options)
  end

  def get_ride_estimate(origin_lat, origin_long, destination_lat, destination_long)
    response = HTTParty.get("#{LYFT_URL}cost?start_lat=#{origin_lat}&start_lng=#{origin_long}&end_lat=#{destination_lat}
                            &end_lng=#{destination_long}&ride_type=lyft",
                             :headers => {"Authorization" => "Bearer #{@access_token}"})
  end

  def get_user_profile
    response = HTTParty.get("#{LYFT_URL}profile",
                            :headers => {"Authorization" => "Bearer #{@access_token}"})
  end

end
