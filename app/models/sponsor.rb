class Sponsor < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable,
         :omniauthable, :omniauth_providers => [:lyft]

  def self.from_omniauth(auth)
    token = auth.credentials['token']
		api = LyftApi.new(token)
		profile = api.get_user_profile.parsed_response
    user = Sponsor.where(provider: auth.provider, email: profile['email']).first_or_create
    user.provider = auth.provider
    user.first_name = profile['first_name']
    user.last_name = profile['last_name']
    user.lyft_access_token = auth.credentials['token']
    user.password = Devise.friendly_token[0,20]
    user.save!
    user
  end
end
