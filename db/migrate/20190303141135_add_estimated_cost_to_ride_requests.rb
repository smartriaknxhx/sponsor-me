class AddEstimatedCostToRideRequests < ActiveRecord::Migration[5.2]
  def change
    add_column :ride_requests, :estimated_cost, :string
  end
end
