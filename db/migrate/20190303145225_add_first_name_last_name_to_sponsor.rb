class AddFirstNameLastNameToSponsor < ActiveRecord::Migration[5.2]
  def change
    add_column :sponsors, :first_name, :string
    add_column :sponsors, :last_name, :string
  end
end
