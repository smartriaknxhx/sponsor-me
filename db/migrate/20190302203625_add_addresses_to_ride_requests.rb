class AddAddressesToRideRequests < ActiveRecord::Migration[5.2]
  def change
    add_column :ride_requests, :start_loc_name, :string, :null => false
    add_column :ride_requests, :start_street_address, :string,:null => false
    add_column :ride_requests, :start_city, :string,:null => false
    add_column :ride_requests, :start_zip, :string,:null => false
    add_column :ride_requests, :dest_loc_name, :string,:null => false
    add_column :ride_requests, :dest_street_address, :string,:null => false
    add_column :ride_requests, :dest_city, :string, :null => false
    add_column :ride_requests, :dest_zip, :string, :null => false
  end
end
