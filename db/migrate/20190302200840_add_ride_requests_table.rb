class AddRideRequestsTable < ActiveRecord::Migration[5.2]
  def change
    create_table  :ride_requests do |t| 
       t.string    :start_lat, null: false
       t.string    :start_long, null: false
       t.string    :destination_lat, null: false
       t.string    :destination_long, null: false
       t.datetime :start_time
       t.timestamps
     end 
  end
end
