class AddLastNameToRiders < ActiveRecord::Migration[5.2]
  def change
    add_column :riders, :last_name, :string, null: false
  end
end
