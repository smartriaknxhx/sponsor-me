class AddDefaultToStatusColumn < ActiveRecord::Migration[5.2]
  def change
    change_column_default :ride_requests, :status, 'open'
  end
end
