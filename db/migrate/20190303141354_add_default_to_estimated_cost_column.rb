class AddDefaultToEstimatedCostColumn < ActiveRecord::Migration[5.2]
  def change
    change_column_default :ride_requests, :estimated_cost, '0.00'
  end
end
