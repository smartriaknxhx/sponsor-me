class AddPhoneNumberToRiders < ActiveRecord::Migration[5.2]
  def change
    add_column :riders, :phone_number, :string, null: false
    add_column :riders, :first_name, :string, null: false
  end
end
