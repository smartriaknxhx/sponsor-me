class AddStatusToRideRequest < ActiveRecord::Migration[5.2]
  def change
    add_column :ride_requests, :status, :string
  end
end
