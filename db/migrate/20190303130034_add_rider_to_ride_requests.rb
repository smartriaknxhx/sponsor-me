class AddRiderToRideRequests < ActiveRecord::Migration[5.2]
  def change
    remove_column :ride_requests, :rider_id
    add_reference :ride_requests, :rider, foreign_key: true
  end
end
