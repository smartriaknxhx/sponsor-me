class AddRideRequestIdToRideCoordsTable < ActiveRecord::Migration[5.2]
  def change
    add_column :ride_coords, :ride_request_id, :string
  end
end
