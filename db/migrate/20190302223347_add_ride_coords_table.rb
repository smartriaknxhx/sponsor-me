class AddRideCoordsTable < ActiveRecord::Migration[5.2]
  def change
    create_table :ride_coords do |t|
      t.string "start_lat", null: false
      t.string "start_long", null: false
      t.string "destination_lat", null: false
      t.string "destination_long", null: false
    end
  end
end
