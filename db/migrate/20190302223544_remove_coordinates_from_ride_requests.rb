class RemoveCoordinatesFromRideRequests < ActiveRecord::Migration[5.2]
  def change
    remove_column :ride_requests, :start_lat
    remove_column :ride_requests, :start_long
    remove_column :ride_requests, :destination_lat
    remove_column :ride_requests, :destination_long
    add_column :ride_requests, :rider_id, :integer, null: false
  end
end
