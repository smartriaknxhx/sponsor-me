class AddLyftTokenToSponsor < ActiveRecord::Migration[5.2]
  def change
    add_column :sponsors, :lyft_access_token, :string
  end
end
