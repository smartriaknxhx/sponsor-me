class AddColumnsToSponsors < ActiveRecord::Migration[5.2]
  def change
    add_column :sponsors, :provider, :string
  end
end
