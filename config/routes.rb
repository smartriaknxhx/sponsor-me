Rails.application.routes.draw do
  devise_for :organizers
  devise_for :riders
  devise_for :sponsors, :controllers => { :omniauth_callbacks => "callbacks" }
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root to: "home#index"

  get "ride_requests", to: "sponsors#ride_requests", as: :ride_requests

  match "request_ride", to: "riders#request_ride", as: :request_ride, via: [:get, :post]
  get "sponsor_ride", to: "requests#sponsor_ride", as: :sponsor_ride
  post "create_ride_request", to: "riders#create_ride_request", as: :create_ride_request

  resources :riders
  resources :organizers
	resources :sponsors

end
