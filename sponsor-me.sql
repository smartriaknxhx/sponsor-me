DELETE FROM ride_coords;
DELETE FROM ride_requests;
DELETE FROM riders;

INSERT INTO riders (id, first_name, last_name, phone_number, created_at, updated_at) VALUES
  (1, "Will", "Wilson", "8659787582", CURRENT_TIMESTAMP, CURRENT_TIMESTAMP),
  (2, "Stevana","Fackney","4505988173", CURRENT_TIMESTAMP, CURRENT_TIMESTAMP),
  (3, "Judi","Cragell","3479178462", CURRENT_TIMESTAMP, CURRENT_TIMESTAMP),
  (4, "Guillermo","Gaddes","9237229154", CURRENT_TIMESTAMP, CURRENT_TIMESTAMP),
  (5, "Polly","Jozwiak","4321382991", CURRENT_TIMESTAMP, CURRENT_TIMESTAMP),
  (6, "Verina","Broader","4191703697", CURRENT_TIMESTAMP, CURRENT_TIMESTAMP),
  (7, "Shaw","Macilhench","4796953952", CURRENT_TIMESTAMP, CURRENT_TIMESTAMP),
  (8, "Sigfried","Carlyle","9157388920", CURRENT_TIMESTAMP, CURRENT_TIMESTAMP),
  (9, "Garik","Curror","3911257018", CURRENT_TIMESTAMP, CURRENT_TIMESTAMP),
  (10, "Cherri","Drohan","4298788803", CURRENT_TIMESTAMP, CURRENT_TIMESTAMP),
  (11, "Catina","Steen","5139356651", CURRENT_TIMESTAMP, CURRENT_TIMESTAMP),
  (12, "Tyrone","Croutear","6131517838", CURRENT_TIMESTAMP, CURRENT_TIMESTAMP),
  (13, "Yance","Duckit","2468971651", CURRENT_TIMESTAMP, CURRENT_TIMESTAMP),
  (14,"Jermaine","Sidworth","8981711178", CURRENT_TIMESTAMP, CURRENT_TIMESTAMP),
  (15, "Ronnica","Aizikowitz","6424461342", CURRENT_TIMESTAMP, CURRENT_TIMESTAMP),
  (16, "Benjie","Beeswing","6413533638", CURRENT_TIMESTAMP, CURRENT_TIMESTAMP),
  (17, "Keith","MacFarland","8985770152", CURRENT_TIMESTAMP, CURRENT_TIMESTAMP),
  (18, "Daphna","Swanborrow","5151787271", CURRENT_TIMESTAMP, CURRENT_TIMESTAMP),
  (19, "Belinda","McNee","7471233667", CURRENT_TIMESTAMP, CURRENT_TIMESTAMP),
  (20, "Jessey","Fortie","9498438291", CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);

INSERT INTO ride_requests (
	id,
	start_time,
  start_loc_name,
  start_street_address,
  start_city,
  start_zip,
  dest_loc_name,
  dest_street_address,
  dest_city,
  dest_zip,
  rider_id,
  created_at,
  updated_at
) VALUES
  (1, "2019-03-06 12:41:56", "Knoxville DMV", "7320 Region Ln", "Knoxville", "37914", "Kroger", "4414 Asheville Hwy", "Knoxville", "37914", 15, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP),
  (2, "2019-03-03 15:30:00", "Walgreens", "1725 Cumberland Ave", "Knoxville", "37916", "Home", "1300 Clinch Ave", "Knoxville", "37916", 2, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP),
  (3,"2019-04-11 08:11:00", "Home", "1300 Clinch Ave", "Knoxville", "37916", "Movies!", "510 S Gay St", "Knoxville", "37902", 8, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP),
  (4,"2019-03-03 14:00:00", "Home", "1300 Clinch Ave", "Knoxville", "37916", "Hospital", "1901 W Clinch Ave", "Knoxville", "37916", 1, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP),
  (5,"2019-05-23 16:45:00", "Knoxville VA", "8033 Ray Mears Blvd", "Knoxville", "37919", "Home", "1300 Clinch Ave", "Knoxville", "37916", 20, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP),
  (6, "2019-03-06 12:41:56", "Knoxville DMV", "7320 Region Ln", "Knoxville", "37914", "Kroger", "4414 Asheville Hwy", "Knoxville", "37914", 15, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);


INSERT INTO ride_coords
(id,
start_lat,
start_long,
destination_lat,
destination_long,
ride_request_id)
VALUES
(1, '35.93731229999999', '-83.89004919999999', '35.9237826', '-84.0477397', 1),
(2, '35.9763', '-83.9319551', '35.9237826', '-84.0477397', 2),
(3, '35.93731229999999', '-83.89004919999999', '35.9237826', '-84.0477397', 3),
(4, '35.93731229999999', '-83.89004919999999', '35.9237826', '-84.0477397', 4),
(5, '35.93731229999999', '-83.89004919999999', '35.9237826', '-84.0477397', 5),
(6, '35.93731229999999', '-83.89004919999999', '35.9237826', '-84.0477397', 6)
;


